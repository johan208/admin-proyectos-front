import {TAREAS_PROYECTO,
        AGREGAR_TAREA,
        VALIDAR_TAREA,
        ELIMINAR_TAREA,
        TAREA_ACTUAL,
        ACTUALIZAR_TAREA} from '../../types';

export default (state,action) => {
    switch(action.type) {
        case TAREAS_PROYECTO:
            return {
                ...state,
                tareas: action.payload
            }
        case AGREGAR_TAREA:
            return {
                ...state,
                tareas:[...state.tareas,action.payload],
                errortarea: false
            }
        case VALIDAR_TAREA:
            return {
                ...state,
                errortarea:true
            }
        case ELIMINAR_TAREA:
            return {
                ...state,
                tareas:state.tareas.filter(tarea => tarea._id !== action.payload)
            }
        case ACTUALIZAR_TAREA:
            return {
                ...state,
                tareas: state.tareas.map(tarea => tarea._id === action.payload._id ? action.payload : tarea),
                tareaeditar:null
            }
        case TAREA_ACTUAL:
            return {
                ...state,
                tareaeditar:action.payload,

            }
        default:
            return state;
    }
}