import React, {useReducer} from 'react';
import clienteAxios from './../../config/axios';

import tareasContext from './tareasContext';
import TareasReducer from './tareasReducer';
import {TAREAS_PROYECTO,
        AGREGAR_TAREA,
        VALIDAR_TAREA,
        ELIMINAR_TAREA,
        TAREA_ACTUAL,
        ACTUALIZAR_TAREA} from '../../types';

const TareasState = (props) => {
    

    const initialState = {
        tareas: [],
        tarea: '',
        errortarea:false,
        tareaeditar: null

    }

    //crear dispatch y state
    const [state, dispatch] = useReducer(TareasReducer, initialState);

    //obtener tareas del proyecto
    const obtenerTareas = async proyecto => {
        try {
            const resultado = await clienteAxios.get('/api/tareas', {params: { proyecto }});
            console.log(resultado);
            dispatch({
                type: TAREAS_PROYECTO,
                payload: resultado.data.tareas
            })

        } catch (error) {
            console.log(error)
        }       
    }

    //AGREGAR TAREA
    const agregarTarea = async tarea => {
        try {
            const resultado = await clienteAxios.post(`/api/tareas`, tarea)
            console.log(resultado);
            dispatch({
                type:AGREGAR_TAREA,
                payload:tarea
            })
        } catch (error){
            console.log(error.response);
        }
    }

    //VALIDAR TAREA
    const validarTarea = () => {
        dispatch({
            type:VALIDAR_TAREA,

        })
    }
    //ELIMINAR TAREA
    const eliminarTarea = async (tareaId,proyecto) => {
        try {
            await clienteAxios.delete(`/api/tareas/${tareaId}`, { params: { proyecto }})
            dispatch({
                type: ELIMINAR_TAREA,
                payload: tareaId
            })
        } catch (error) {
            console.log(error);
        }
    }

    // EXTRAER UNA TAREA PARA EDITAR
    const guardarTareaActual = tarea => {
        dispatch({
            type: TAREA_ACTUAL,
            payload: tarea
        })
    }

    //EDITA TAREA
    const editarTarea = async tarea => {
        try {
            const resultado = await clienteAxios.put(`/api/tareas/${tarea._id}`, tarea)
            console.log(resultado);
            dispatch({
                type: ACTUALIZAR_TAREA,
                payload:resultado.data.tarea
            })

        } catch(error) {
            console.log(error.response)
        }
        
    }

    return ( 
        <tareasContext.Provider
            value={{
                tareas:state.tareas,
                errortarea:state.errortarea,
                tareaeditar:state.tareaeditar,
                obtenerTareas,
                agregarTarea,
                validarTarea,
                eliminarTarea,
                guardarTareaActual,
                editarTarea
            }}
        >
            {props.children}
        </tareasContext.Provider>
     )
}
 
export default TareasState;