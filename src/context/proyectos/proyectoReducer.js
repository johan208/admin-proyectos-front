import { FORMULARIO_PROYECTO,
    OBTENER_PROYECTOS,
    AGREGAR_PROYECTO,
    VALIDAR_NPROYECTO,
    PROYECTO_ACTUAL,
    ELIMINAR_PROYECTO,
    PROYECTO_ERROR } from '../../types';

export default (state, action) => {
    switch(action.type) {
        case FORMULARIO_PROYECTO:
            return {
                ...state,
                nuevoProyectoForm: !state.nuevoProyectoForm
            }
        case OBTENER_PROYECTOS:
            // al state proyectos le pasamos el payload
            // que es un array definido previamente en el archivo ProyectoState
            return {
                ...state,
                proyectos: action.payload
            }
        case AGREGAR_PROYECTO:
        //hace una copia del array proyectos
        //y mete el nuevo item en ese array
        return {
            ...state,
            proyectos: [...state.proyectos, action.payload],
            nuevoProyectoForm: false,
            errorFormNP: false
        }

        case VALIDAR_NPROYECTO:

        return {
            ...state,
            errorFormNP: true
        }

        case PROYECTO_ACTUAL:
            return {
                ...state,
                proyectoActual: state.proyectos.filter(proyecto => proyecto._id === action.payload)
                 
            }

        case ELIMINAR_PROYECTO:
            return {
                ...state,
                proyectos: state.proyectos.filter(proyecto => proyecto._id !== action.payload ),
                proyectoActual: null
            }
        case PROYECTO_ERROR:
            return {
                ...state,
                mensaje: action.payload
            }
        default:
            return state;
    }
}