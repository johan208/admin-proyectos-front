import React, { useReducer} from 'react';
import clienteAxios from '../../config/axios';

import proyectoReducer from './proyectoReducer'
import proyectoContext from './proyectoContext';
import { FORMULARIO_PROYECTO,
         OBTENER_PROYECTOS,
         AGREGAR_PROYECTO,
         VALIDAR_NPROYECTO,
         PROYECTO_ACTUAL,
         ELIMINAR_PROYECTO,
         PROYECTO_ERROR  } from '../../types';



const ProyectoState = props => {

    const initialState = {
        proyectos: [],
        nuevoProyectoForm: false,
        errorFormNP:false,
        proyectoActual:null,
        mensaje:null
    }

    // Dispatch para ejecutar acciones
    const [state, dispatch] = useReducer( proyectoReducer, initialState);
    
    // serie de funcionaes para CRUD
    
    const mostrarFormulario = () => {
        dispatch({
            type: FORMULARIO_PROYECTO
        })
    }

    const obtenerProyectos = async () => {
        try {
            const respuesta = await clienteAxios.get('/api/proyectos');
            dispatch({
                // payload es lo que toma la funcion como parametro
                type: OBTENER_PROYECTOS,
                payload: respuesta.data.proyectos
            })
        } catch (error) {
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }
            dispatch({
                type:PROYECTO_ERROR,
                payload:alerta
            })
        }
    }

    // agregar nuevo proy
    const agregarProyecto = async (proyecto) => {
        try {   
            const respuesta = await clienteAxios.post('/api/proyectos', proyecto)
            console.log(respuesta);

            // Insertar proyecto al state
            dispatch({
                type:AGREGAR_PROYECTO,
                payload: respuesta.data
            })
        } catch(error) {
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }
            dispatch({
                type:PROYECTO_ERROR,
                payload:alerta
            })
        }

        
    }

    //validar formulario nuevo proyecto
    const mostrarError = () => {
        dispatch({
            type: VALIDAR_NPROYECTO
        })
    }

    //seleccionar proyecto actual

    const proyectoActualFunc = proyectoId => {
            dispatch({
                type:PROYECTO_ACTUAL,
                payload: proyectoId
            })
    }

    // eliminar proyecto
    const eliminarProyecto = async proyectoId => {
        try {
            await clienteAxios.delete(`/api/proyectos/${proyectoId}`)
            dispatch({
                type: ELIMINAR_PROYECTO,
                payload: proyectoId 
            })
        } catch (error) {
            console.log(error.response)
            const alerta = {
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }
            dispatch({
                type:PROYECTO_ERROR,
                payload:alerta
            })
        }
        
    }

    return (
        <proyectoContext.Provider
            value={{
                proyectos: state.proyectos,
                nuevoProyectoForm: state.nuevoProyectoForm,
                errorFormNP: state.errorFormNP,
                proyectoActual: state.proyectoActual,
                mensaje:state.mensaje,
                mostrarFormulario,
                obtenerProyectos,
                agregarProyecto,
                mostrarError,
                proyectoActualFunc,
                eliminarProyecto
            }}
        >
            {props.children}
        </proyectoContext.Provider>
    )
}

export default ProyectoState;