import React, {useContext, useState, useEffect} from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareasContext from '../../context/tareas/tareasContext';


const FormTarea = () => {

    //extaer si un proyecto esta activo
    const proyectosContext = useContext(proyectoContext);
    const { proyectoActual} = proyectosContext;
    
    //obtener la funcion del context tarea
    const tareaContext = useContext(tareasContext);
    const { tareaeditar, errortarea, obtenerTareas, agregarTarea, validarTarea, editarTarea} = tareaContext;


    //state del formulario
    const [tarea,guardarTarea] = useState({
        nombre:''
    });
    const {nombre } = tarea;

    //effect que detecta si hay tarea a editar
    useEffect(() => {
        if(tareaeditar !== null) {
            guardarTarea(tareaeditar)
        } else {
            guardarTarea({
                nombre:''
            })
        }

    }, [tareaeditar])

    if(!proyectoActual) return null

    const [proyectoActivo] = proyectoActual;

    //leer los valores del formulario
    const handleChage= e => {
        guardarTarea({
            ...tarea,
            [e.target.name]: e.target.value
        })
    }

    const onSubmitFormTarea = e => {
        e.preventDefault();

        //validar
        if(nombre.trim() === '') {
            validarTarea();
            return;
        }
        //si es editar o agregar
        if(tareaeditar === null) {
            //tarea nueva

            //agregar nueva tarea a tareass
            tarea.proyecto = proyectoActivo._id;
            agregarTarea(tarea);
        } else {
            //actualizar o editar tarea
            editarTarea(tarea);
        }
        //obtener y filtrar las tareas nuevamente cuando se agrega una nueva
        obtenerTareas(proyectoActivo._id);

        //reiniciar form
        guardarTarea({
            nombre: ''
        })
    }
    
    
    return ( 
        <div className="formulario" >
            <form action=""
                onSubmit={onSubmitFormTarea}
            >
                <div className="contenedor-input">
                    <input type="text" className="input-text"
                        placeholder="Nombre Tarea ..."
                        name="nombre"
                        onChange={handleChage}
                        value={nombre}
                    />
                </div>

                <div className="contenedor-input">
                    <input type="submit"
                        className="btn btn-primario btn-submit btn-block"
                        value={tareaeditar ? 'EditarTarea' : 'AgregarTarea'}
                    /> 
                </div>
            </form>

            { errortarea ? <p className="mensaje error">El nombre de la tarea es obligatorio</p> : null }
        </div>
     );
}
 
export default FormTarea;