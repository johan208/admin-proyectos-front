import React, { Fragment, useContext } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareasContext from '../../context/tareas/tareasContext';

import { CSSTransition, TransitionGroup } from 'react-transition-group'
import Tarea from './Tarea';



const ListaTareas = () => {

    //extraer proyectos al state inicial proyecto
    const proyectosContext = useContext(proyectoContext);
    const { proyectoActual, eliminarProyecto} = proyectosContext;
    
    // extraer tareas al state inicial
    const tareaContext = useContext(tareasContext);
    const { tareas} = tareaContext;

    //si no hay proyecto seleccionado
    if(!proyectoActual) return <h2>Selecciona un proyecto</h2>

    // array destructuring para extraer el proyecto
    const [proyectoActivo] = proyectoActual;

    //eliminar proyecto
    const onClickEliminar = () => {
        eliminarProyecto(proyectoActivo._id)
    }

    return ( 
        <Fragment>
            <h2>PROYECTO: {proyectoActivo.nombre}</h2>

            <ul className="listado-tareas">
                {tareas.length === 0
                    ?   (<li className="tarea"><p>No hay Tareas</p></li>)
                    :   
                    <TransitionGroup>
                        {tareas.map( tarea => (
                            <CSSTransition
                            key={tarea._id}
                            timeout={200}
                            classNames="tarea"
                            >
                                <Tarea 
                                    
                                    tarea={tarea}
                                />
                            </CSSTransition>
                            
                        ))}
                        
                    </TransitionGroup>
                }
            </ul>

            <button
                type="button"
                className="btn btn-eliminar" 
                onClick= {onClickEliminar}   
            >Eliminar Proyecto &times;</button>
            
        </Fragment>
     );
}
 
export default ListaTareas;