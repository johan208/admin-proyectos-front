import React, { useContext } from 'react';
import tareasContext from '../../context/tareas/tareasContext';
import proyectoContext from '../../context/proyectos/proyectoContext';


const Tarea = ({tarea}) => {
    //proyecto context
    const proyectosContext = useContext(proyectoContext);
    const { proyectoActual } = proyectosContext;
    //tareas context
    const tareaContext = useContext(tareasContext);
    const { obtenerTareas, eliminarTarea, editarTarea, guardarTareaActual } = tareaContext;

    //funcion  cuando presionan btn eliminar
    const eliminarTareas = (id) => {
        eliminarTarea(id,proyectoActual[0]._id);

        //refrescar la vista de tareas del proyecto seleccionado
        //proyecto actual es un array
        obtenerTareas(proyectoActual[0]._id);
    }

    //funcion que modifica estado tareas
    const cambiarEstado = (tarea) => { 
            tarea.estado = !tarea.estado
            editarTarea(tarea)
    }

    // selecciona una tarea para editarla
    const seleccionarTarea = (tarea) => {

        //
        guardarTareaActual(tarea);
    }

    return ( 
        <li className="tarea sombra">
            <p> {tarea.nombre} </p>

            <div className="estado">
                {tarea.estado 
                ?   
                    (
                        <button 
                            type="button"
                            className="completo"
                            onClick={() => cambiarEstado(tarea)}    
                        >Completo </button>
                    )
                :   
                    (
                        <button 
                            type="button"
                            className="incompleto" 
                            onClick={() => cambiarEstado(tarea)}   
                        >Incompleto </button>
                    )
                }
            </div>

            <div className="acciones">
                <button
                    className="btn btn-primario"
                    type="button"
                    onClick={() => seleccionarTarea(tarea)}
                >Editar</button>

                <button
                    className="btn btn-primario"
                    type="button"
                    onClick={() => eliminarTareas(tarea._id)}
                >Eliminar</button>
            </div>
        </li>
     );
}
 
export default Tarea;