import React, { useState, useContext } from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';

const NuevoProyecto = () => {

    // obtener el state del formulario (la constante debe llamarse diferente del context !!!!)
    const proyectosContext = useContext(proyectoContext);
    const {nuevoProyectoForm,errorFormNP, mostrarFormulario, agregarProyecto, mostrarError} = proyectosContext;


    //state para proyecto
    const [proyecto, guardarProyecto] = useState({
        nombre: ''
    });
    
    //extraer nombre del proyecto
    const {nombre} = proyecto;

    // lee lso contenidos del input
    const onChangeProyecto = e => {
        guardarProyecto({
            ...proyecto,
            [e.target.name ] : e.target.value
        })
    }

    // cuando el user registra el proyecto
    const onSubmitProyecto = e => {
        e.preventDefault();

        // Validar el proyect
        if(nombre === ''){
            mostrarError();
            return;}
         


        // Agregar el state
        agregarProyecto(proyecto);

        // Reiniciar el form
        guardarProyecto({
            nombre: ''
        })
    }
    
    // Mostrar form
    const onClickShowForm = () => {
        mostrarFormulario();
    }
    
    return ( 
        <>
            <button type="button" className="btn btn-block btn-primario"
                onClick={ onClickShowForm }
            >
                Nuevo Proyecto
            </button>

            {
                nuevoProyectoForm ?
                   (
                        <form className="formulario-nuevo-proyecto" 
                        onSubmit={ onSubmitProyecto }    
                        >
                            <input 
                                type="text"
                                className="input-text"
                                placeholder="Nombre Proyecto" 
                                name="nombre" 
                                onChange={ onChangeProyecto }
                            />

                            <input 
                            type="submit" 
                            className="btn btn-primario btn-block"
                            value="Agregar proyecto"
                            />
                        </form>
                    )   : null }
                {errorFormNP ? <p className="mensaje error">Proyecto es Obligatorio*</p> : null }
        </>
     );
}
 
export default NuevoProyecto;