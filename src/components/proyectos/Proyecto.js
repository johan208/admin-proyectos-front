import React, {useContext} from 'react';
import proyectoContext from '../../context/proyectos/proyectoContext';
import tareasContext from '../../context/tareas/tareasContext';

const Proyecto = ({proyecto}) => {
    //obtener state de proyectos
    const proyectosContext = useContext(proyectoContext);
    const {proyectoActualFunc} = proyectosContext;
    //obtener state y context de tareas
    const tareaContext = useContext(tareasContext);
    const {obtenerTareas} = tareaContext;

    // Funcion para el proyecto que se vera
    const seleccionarProyecto = (id) => {
        proyectoActualFunc(id); //fijar un proyecto para visualizar
        obtenerTareas(id); // filtrar tareas cuando se de click
    }
    return ( 
        <li>
            <button
                type="button"
                className="btn btn-blank"
                onClick={() => seleccionarProyecto(proyecto._id)}
            >
                {proyecto.nombre}
            </button>
        </li>
     );
}
 
export default Proyecto;