import React, { useContext, useEffect } from 'react';
import Proyecto from './Proyecto';
import AlertaContext from '../../context/alerta/alertaContext'
import proyectoContext from '../../context/proyectos/proyectoContext'
import { CSSTransition, TransitionGroup } from 'react-transition-group'

const ListaProyectos = () => {

    const proyectosContext = useContext(proyectoContext);
    const { proyectos, mensaje, obtenerProyectos} = proyectosContext;

    const alertaContext = useContext(AlertaContext);
    const {alerta, mostrarAlerta} = alertaContext;

    // antes de un useEffect no debe haber ningun return!!
    // Obtener proyectos cuando carga el componente
    useEffect( () => {

        if(mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria)
        }
        obtenerProyectos();
    }, [mensaje])

    if(proyectos.length === 0) return <p>No hay proyectos, comienza creando uno</p>;

    

    return ( 
        <ul className="listado-proyectos">

            { alerta ? ( <div className={`alerta ${alerta.categoria}`}>{alerta.msg}</div>) : null }

            <TransitionGroup>
                {proyectos.map(proyecto => (
                    <CSSTransition
                        key={proyecto._id}
                        timeout={200}
                        classNames="tarea"
                    >
                        <Proyecto 
                        proyecto={proyecto}
                    />
                    </CSSTransition>
                    
                ))}
            </TransitionGroup>
            
        </ul>
     );
}
 
export default ListaProyectos;