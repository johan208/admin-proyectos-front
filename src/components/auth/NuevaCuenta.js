import React, { useContext, useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

import AlertaContext from './../../context/alerta/alertaContext';
import AuthContext from './../../context/autenticacion/authContext';


const NuevaCuenta = (props) => {
    //Extraer valores del context
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta} = alertaContext;

    const authContext = useContext(AuthContext);
    const {mensaje, autenticado, registrarUsuario } = authContext;

    // En caso de que el usuario se haya registrado o este duplciado el correo
    useEffect( () => {
        if( autenticado ) {
            props.history.push('/proyectos');
        }

        if( mensaje ) {
            mostrarAlerta(mensaje.msg, mensaje.categoria);
            
        }

    }, [mensaje, autenticado, props.history])

    // State para iniciar Sesion
    const [usuario, guardarUsuario] = useState({
        nombre: '',
        email:'',
        password:'',
        confirmar: ''
    });
    const { nombre ,email, password, confirmar } = usuario;

    const onChange = (e) => {
        guardarUsuario({ 
            ...usuario,
            [e.target.name] : e.target.value
        });
    }

    const Registro = (e) => {
        e.preventDefault();

        //validar que no halla campos vacios
        if(nombre.trim() === '' || email.trim() === '' || password.trim() === '' || confirmar.trim() === '' ) {
            mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
            return;
        }

        // Password minimod e 6 caracteres
        if(password.length <= 5) {
            mostrarAlerta('El password debe ser almenos 6 caracteres', 'alerta-error');
            return;
        }
        // los 2 password sean iguales
        if(password !== confirmar) {
            mostrarAlerta('Los password no coinciden', 'alerta-error');
            return;
        }
        // Pasarlo al action
        registrarUsuario({
            nombre,
            email,
            password
        })
    }

    return ( 
        <div className="form-usuario">
            { alerta ? ( <div className={`alerta ${alerta.categoria }`}> {alerta.msg} </div> ) : null}
            <div className="contenedor-form sombra-dark">
                <h1>Nueva cuenta</h1>

                <form onSubmit={Registro}>
                    <div className="campo-form">
                        {/*jsx htmlFor === for en html */}
                        <label htmlFor="nombre">Nombre</label>
                        <input 
                            type="text"
                            id="nombre"
                            name="nombre"
                            placeholder="Tu Nombre Completo"
                            value={nombre}
                            onChange={onChange} 
                        />
                    </div>
                    <div className="campo-form">
                        {/*jsx htmlFor === for en html */}
                        <label htmlFor="email">Email</label>
                        <input 
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tu Email"
                            value={email}
                            onChange={onChange} 
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password">Contraseña</label>
                        <input 
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tu Password"
                            value={password}
                            onChange={onChange} 
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="confirmar">Confirmar Contraseña</label>
                        <input 
                            type="password"
                            id="confirmar"
                            name="confirmar"
                            placeholder="Repite tu Password"
                            value={confirmar}
                            onChange={onChange} 
                        />
                    </div>
                    <div className="campo-form">
                        <input 
                            type="submit" className="btn btn-primario btn-block"
                            value="Registrarme"
                        />
                    </div>
                </form>
                <Link to={'/'}>Volver a iniciar Sesión</Link>
            </div>
        </div>
     );
}
 
export default NuevaCuenta;